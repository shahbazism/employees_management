from django.contrib import admin
from legal_receipts import models

class ReceiptInline(admin.TabularInline):
    model = models.Receipt

class EmployeeAdmin(admin.ModelAdmin):
    inlines = [
        ReceiptInline,
    ]

# Register your models here.
admin.site.register(models.Employee, EmployeeAdmin)
admin.site.register(models.Receipt)
