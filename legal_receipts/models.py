from django.db import models

# Create your models here.
from django.db.models import PROTECT


class Employee(models.Model):
    # choices variable
    age_choices = [(num, age) for num in range(0, 80) for age in range(20, 100)]

    # variables of class
    first_name = models.CharField(max_length=100, verbose_name='نام')
    last_name = models.CharField(max_length=100, verbose_name='نام خانوادگی')
    personal_code = models.CharField(max_length=4, unique=True, blank=False, null=False, verbose_name='کد پرسنلی')
    national_code = models.CharField(max_length=10, unique=True, blank=False, null=False, verbose_name='کد ملی')
    age = models.IntegerField(choices=age_choices, verbose_name='سن')

    # keys of class
        # DocS
    # receipt = models.ForeignKey('Receipt', on_delete=PROTECT)


class Receipt(models.Model):

    # choices variable
    month_choices = [(1, 'فروردین'), (2, 'اردیبهشت'), (3, 'خرداد'), (4, 'تیر'), (5, 'مرداد'), (6, 'شهریور'), (7, 'مهر'), (8, 'آبان'), (9, 'آذر'), (10, 'دی'), (11, 'بهمن'), (12, 'اسفند')]
    year_choices = [(num, year) for num in range(0, 20) for year in range(1390, 1410)]

    # variables of class
    month = models.IntegerField(choices=month_choices, verbose_name='ماه')
    year = models.IntegerField(choices=year_choices, verbose_name='سال')

    # keys of class
    employee = models.ForeignKey('Employee', on_delete=PROTECT, verbose_name='کارمند')
