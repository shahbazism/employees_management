from django.apps import AppConfig


class LegalReceiptsConfig(AppConfig):
    name = 'legal_receipts'
