from django.urls import path

from legal_receipts import views

urlpatterns = [
    path('', views.index, name='legal_receipts_index'),
]